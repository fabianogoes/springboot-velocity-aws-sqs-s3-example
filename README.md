# Demo Velocity, AWS SQS, AWS S3



### SQS

[Alpine SQS (alpine-sqs)](https://hub.docker.com/r/roribio16/alpine-sqs/)

**Dependency AWS CLI**

```
pip3 install awscli --upgrade --user
aws --version

aws configure
AWS Access Key ID [None]: 123
AWS Secret Access Key [None]: 123
Default region name [None]: us-east-1
Default output format [None]:
```


**Run Docker SQS**

```
docker run --name alpine-sqs -p 9324:9324 -p 9325:9325 -d roribio16/alpine-sqs:latest
``` 

**Create new Queue**

```
aws --endpoint-url http://localhost:9324 sqs create-queue --queue-name b2b-billing-recibo
```

