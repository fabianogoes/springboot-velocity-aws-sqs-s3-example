package com.example.velocitydemo.api;

import org.apache.commons.io.FileUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;

@RestController
@RequestMapping("/")
public class MainController {

    @Autowired
    private VelocityEngine velocityEngine;

    @GetMapping
    public String get() throws IOException {
        VelocityContext context = new VelocityContext();
        context.put("title", "Testando exportação de html com velocity");
        context.put("body", "Este é o corpo do html");
        StringWriter stringWriter = new StringWriter();
        Template template = velocityEngine.getTemplate("templates/recibo.vm");
        template.merge(context, stringWriter);
        FileUtils.writeStringToFile(new File("recibo.html"), stringWriter.toString(), "UTF-8");
        return "Velocity Html Export";
    }

}
