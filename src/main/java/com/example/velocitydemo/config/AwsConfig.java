package com.example.velocitydemo.config;

import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.destination.DynamicDestinationResolver;

import javax.jms.Session;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
@EnableJms
public class AwsConfig {

    @Value("aws.credentials.access-key-id")
    private String awsAccessKeyId;

    @Value("aws.credentials.secret-key")
    private String awsSecretKey;

    private AWSCredentialsProvider credentialsProvider() {
        return new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return credentials();
            }

            @Override
            public void refresh() { }
        };
    }

    private AWSCredentials credentials() {
        AWSCredentials awsCredentials = new AWSCredentials() {
            @Override
            public String getAWSAccessKeyId() {
                return awsAccessKeyId;
            }

            @Override
            public String getAWSSecretKey() {
                return awsSecretKey;
            }
        };
        return awsCredentials;
    }

    private SQSConnectionFactory connectionFactory() throws UnknownHostException {
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setLocalAddress(InetAddress.getByName("http://localhost:9324"));

        return SQSConnectionFactory.builder()
                .withRegion(Region.getRegion(Regions.US_EAST_1))
                .withAWSCredentialsProvider(credentialsProvider())
                .withClientConfiguration(clientConfiguration)
                .build();
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() throws UnknownHostException {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(this.connectionFactory());
        factory.setDestinationResolver(new DynamicDestinationResolver());
        factory.setConcurrency("3-10");
        factory.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
        return factory;
    }

    @Bean
    public JmsTemplate defaultJmsTemplate() throws UnknownHostException {
        return new JmsTemplate(this.connectionFactory());
    }

}