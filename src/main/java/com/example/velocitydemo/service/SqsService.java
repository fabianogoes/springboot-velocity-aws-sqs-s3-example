package com.example.velocitydemo.service;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;

@Service
public class SqsService {

    @JmsListener(destination = "b2b-billing-recibo")
    public void createThumbnail(String requestJSON) throws JMSException {
        System.out.println("Received ");
        System.out.println("requestJSON = " + requestJSON);
    }

}
